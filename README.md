# TRACKHANDLER

REST-API Handler for Tracks @ Combat Management System Simulator Game Using :

- Spring Boot
- Reactive Programming
- Apache Kafka
- RabbitMQ
