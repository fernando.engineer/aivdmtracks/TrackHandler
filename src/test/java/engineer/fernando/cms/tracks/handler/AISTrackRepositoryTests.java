package engineer.fernando.cms.tracks.handler;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.CommunicationState;
import engineer.fernando.ais.decoder.messages.types.SyncState;
import engineer.fernando.cms.tracks.handler.model.entity.AISTrack;
import engineer.fernando.cms.tracks.handler.model.entity.AISTrackMessage;
import engineer.fernando.cms.tracks.handler.repository.AISTrackRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
@DataJpaTest
class AISTrackRepositoryTests {

	@Autowired
	AISTrackRepository aisTrackRepository;

	static AISTrack aisTrack1979;
	static AISTrack aisTrack1977;

	@BeforeAll
	static void beforeAll() {
		CommunicationState communicationState = new CommunicationState(SyncState.UTC_DIRECT) {
			@Override
			public SyncState getSyncState() {
				return super.getSyncState();
			}
		};

		aisTrack1979 = new AISTrack();
		aisTrack1979.setMessageType(AISMessageType.SHIP_AND_VOYAGE_RELATED_DATA);
		aisTrack1979.setCommunicationState(communicationState);
		aisTrack1979.setMMSI(1979);
		aisTrack1979.setDestination("Prosperity");

		aisTrack1977 = new AISTrack();
		aisTrack1977.setMessageType(AISMessageType.POSITION_REPORT_CLASS_A_SCHEDULED);
		aisTrack1977.setMMSI(1977);
		aisTrack1977.setLatitude(-23.2f);
		aisTrack1977.setLongitude(-43.4f);
		aisTrack1977.setCommunicationState(communicationState);

	}

	@BeforeEach
	void setUp() {

	}

	@Test
	void testEmptyRepository() {
		List<AISTrack> aisTracks = aisTrackRepository.findAll();
		assertTrue(aisTracks.isEmpty());
	}

	@Test
	void testRepository() {
		// 1st Publication : SHIP_AND_VOYAGE_RELATED_DATA for MMSI 1979
		AISTrack aisTrack = aisTrackRepository.save(aisTrack1979);
		assertEquals(1979,aisTrack.getMMSI());
		assertEquals(AISMessageType.SHIP_AND_VOYAGE_RELATED_DATA,aisTrack.getMessageType());
		assertEquals("Prosperity",aisTrack.getDestination());
		assertEquals(1, aisTrackRepository.count());

		// 2nd Publication : POSITION_REPORT_CLASS_A_SCHEDULED for MMSI 1977
		AISTrack anotherAisTrack = aisTrackRepository.save(aisTrack1977);
		assertEquals(1977,anotherAisTrack.getMMSI());
		assertEquals(AISMessageType.POSITION_REPORT_CLASS_A_SCHEDULED,anotherAisTrack.getMessageType());
		assertEquals(-23.2f,anotherAisTrack.getLatitude(),0.01);
		assertEquals(-43.4f,anotherAisTrack.getLongitude(),0.01);
		assertEquals(2, aisTrackRepository.count());

		//Deleting updates from ship mmsi 1977 shall no remain for 1977
		aisTrackRepository.deleteByMmsi(1977L);
		// , and remains only 1 update (for 1979)
		assertEquals(1, aisTrackRepository.count());
	}
}
