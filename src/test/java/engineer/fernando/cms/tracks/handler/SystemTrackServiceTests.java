package engineer.fernando.cms.tracks.handler;


import engineer.fernando.cms.domainmodel.avro.Position;
import engineer.fernando.cms.domainmodel.avro.SensorTrackType;
import engineer.fernando.cms.domainmodel.avro.Track;
import engineer.fernando.cms.domainmodel.avro.Track.Builder;
import engineer.fernando.cms.tracks.handler.model.entity.SystemTrack;
import engineer.fernando.cms.tracks.handler.repository.SystemTrackRepository;
import engineer.fernando.cms.tracks.handler.repository.TrackTacticalSituationRepository;
import engineer.fernando.cms.tracks.handler.service.KafkaService;
import engineer.fernando.cms.tracks.handler.service.KafkaServiceImpl;
import engineer.fernando.cms.tracks.handler.service.SystemTrackService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
@DataJpaTest
class SystemTrackServiceTests {

    @Autowired
    private SystemTrackRepository systemTrackRepository;

    @Autowired
    private TrackTacticalSituationRepository tacticalSituationRepository;

    private KafkaService kafkaServiceMock = Mockito.mock(KafkaServiceImpl.class);

    @Test
    void testInitSystemTrack() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        SystemTrackService systemTrackService = new SystemTrackService();
        ReflectionTestUtils.setField(systemTrackService,"kafkaService", this.kafkaServiceMock);
        ReflectionTestUtils.setField(
            systemTrackService,
            "systemTrackRepository",
            this.systemTrackRepository);
        ReflectionTestUtils.setField(
            systemTrackService,
            "systemTrackTacticalSituationRepository",
            this.tacticalSituationRepository
        );

        Builder trackBuilder = Track.newBuilder().setSensorType(SensorTrackType.OWNSHIP).setTrackId("F1895")
            .setName("Fortuna1895").setPlayerId(31415);
        Position position = new Position(0.98, 2.3, 0.0);
        trackBuilder.setPosition(position).setTime(System.currentTimeMillis()).setTrackSpec(null)
            .setOrderedCourseOverGround(42.1).setCourseOverGround(0.0).setOrderedSpeedOverGround(23.2)
            .setSpeedOverGround(0.0).setWeaponsAvailable(7).setFuelInGallons(0.0);
        Track avroTrack = trackBuilder.build();

        Method initSystemTrackMethod =
                SystemTrackService.class.getDeclaredMethod("initSystemTrack",Track.class);
        initSystemTrackMethod.setAccessible(true);

        SystemTrack systemTrack = (SystemTrack) initSystemTrackMethod.invoke(systemTrackService, avroTrack);
        assertEquals(31415,systemTrack.getPlayerId());
        assertEquals(1000000,systemTrack.getLastTacticalUpdate().getFuelInGallons(),0.1);
    }



}
