package engineer.fernando.cms.tracks.handler;

import engineer.fernando.cms.domainmodel.avro.SensorTrackType;
import engineer.fernando.cms.tracks.handler.controller.SystemTrackController;
import engineer.fernando.cms.tracks.handler.model.SystemTrackBuilder;
import engineer.fernando.cms.tracks.handler.model.TrackTacticalSituationBuilder;
import engineer.fernando.cms.tracks.handler.model.dao.TrackDAO;
import engineer.fernando.cms.tracks.handler.model.entity.SystemTrack;
import engineer.fernando.cms.tracks.handler.model.entity.TrackSpecification;
import engineer.fernando.cms.tracks.handler.model.entity.TrackTacticalSituation;
import engineer.fernando.cms.tracks.handler.repository.SystemTrackRepository;
import engineer.fernando.cms.tracks.handler.repository.TrackTacticalSituationRepository;
import engineer.fernando.cms.tracks.handler.service.KafkaService;
import engineer.fernando.cms.tracks.handler.service.KafkaServiceImpl;
import engineer.fernando.cms.tracks.handler.service.SystemTrackService;
import engineer.fernando.cms.tracks.handler.util.TrackSpecificationBundle;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.ReflectionTestUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import java.math.BigDecimal;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;


@ActiveProfiles("test")
@DataJpaTest
 class SystemTrackControllerTests {

	@Autowired
	private SystemTrackRepository systemTrackRepository;

	@Autowired
	private TrackTacticalSituationRepository tacticalSituationRepository;



	private KafkaService kafkaServiceMock = Mockito.mock(KafkaServiceImpl.class);

    @Test
    void testGetAllSystemTracks() throws NoSuchFieldException, IllegalAccessException {
        SystemTrackController systemTrackController = new SystemTrackController();
		SystemTrackService systemTrackService = new SystemTrackService();

		Mockito.doNothing().when(kafkaServiceMock)
			.sendAlertMessage(Mockito.anyString(),Mockito.anyString());
		Mockito.doNothing().when(kafkaServiceMock)
			.sendTrackDAOMessage(Mockito.any(ArrayList.class));
		Mockito.doNothing().when(kafkaServiceMock)
			.sendTrackPerformanceDAOMessage(Mockito.any(ArrayList.class));

		ReflectionTestUtils.setField(systemTrackService,"kafkaService", this.kafkaServiceMock);
		ReflectionTestUtils.setField(
			systemTrackService,
			"systemTrackRepository",
			this.systemTrackRepository);
		ReflectionTestUtils.setField(
			systemTrackService,
			"systemTrackTacticalSituationRepository",
			this.tacticalSituationRepository
		);

		ReflectionTestUtils.setField(
			systemTrackController,
			"systemTrackService",
			systemTrackService)
		;


        // Build specification from Ship template.
        TrackSpecification shipSpecification =
                TrackSpecificationBundle.buildTrackSpecification(SensorTrackType.OWNSHIP);

        SystemTrack track = new SystemTrackBuilder()
                .setPlayerId(31415).setSensorTrackType(SensorTrackType.OWNSHIP).setName("Antares")
                .setWeaponsAvailable(5).setOrderedSpeedOverGround(15.3).setOrderedCourseOverGround(314)
                .setTrackTechSpec(shipSpecification).setHit(false)
                .setSystemTrackId(BigDecimal.valueOf(1)).build();


        // Adding first tactical update for track
        TrackTacticalSituation tacticalUpdate = new TrackTacticalSituationBuilder().setSystemTrack(track)
                .setLatitude(23.23).setLongitude(43.43)
                .setCourseOverGround(123.4).setSpeedOverGround(31.4).setdBNoise(20.5).setFuelInGallons(45.8).build();

        // 1st track publication
        systemTrackService.save(track);

        // 2nd track built
        // Build specification from Weapon template.
        TrackSpecification weaponSpecification =
                TrackSpecificationBundle.buildTrackSpecification(SensorTrackType.WEAPON);

        SystemTrack weapon = new SystemTrackBuilder()
                .setPlayerId(31415).setSensorTrackType(SensorTrackType.WEAPON).setName("LittleJoe")
                .setWeaponsAvailable(0).setOrderedSpeedOverGround(1500.3).setOrderedCourseOverGround(180.7)
                .setTrackTechSpec(weaponSpecification).setHit(false)
                .setSystemTrackId(BigDecimal.valueOf(2)).build();

        // Adding first tactical update for track
        TrackTacticalSituation weaponTacticalUpdate = new TrackTacticalSituationBuilder().setSystemTrack(weapon)
                .setLatitude(23.23).setLongitude(43.43)
                .setCourseOverGround(123.4).setSpeedOverGround(31.4).setdBNoise(20.5).setFuelInGallons(45.8).build();

        // 2nd Publication
        systemTrackService.save(weapon);

		//Run updateSystemTracks at least once is needed
		ReflectionTestUtils.invokeMethod(systemTrackService,"updateSystemTracks",null);

        Flux<TrackDAO> responseList = systemTrackController.getSystemTracks(null);
        assertEquals(2, responseList.collectList().block().size());
        Mono<ResponseEntity<TrackDAO>> response = systemTrackController.getTrackById("W2P31415");
        assertEquals(weapon.getSystemTrackId(), response.block().getBody().getTrackId());

    }
}
