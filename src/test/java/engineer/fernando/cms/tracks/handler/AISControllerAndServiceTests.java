package engineer.fernando.cms.tracks.handler;

import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.cms.tracks.handler.controller.AISController;
import engineer.fernando.cms.tracks.handler.model.dao.AISTrackInfoDAO;
import engineer.fernando.cms.tracks.handler.model.entity.AISTrack;
import engineer.fernando.cms.tracks.handler.repository.AISTrackMessageRepository;
import engineer.fernando.cms.tracks.handler.repository.AISTrackRepository;
import engineer.fernando.cms.tracks.handler.service.AISMessageDecoderService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import engineer.fernando.cms.tracks.handler.service.AISService;
import engineer.fernando.cms.tracks.handler.service.KafkaService;
import engineer.fernando.cms.tracks.handler.service.KafkaServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.ReflectionTestUtils;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertTrue;

@ActiveProfiles("test")
@DataJpaTest
class AISControllerAndServiceTests {

	@Autowired
	private AISTrackRepository aisTrackRepository;

	@Autowired
	AISTrackMessageRepository aisTrackMessageRepository;

	private AISMessageDecoderService aisMessageDecoderService = new AISMessageDecoderService();

	private KafkaService kafkaServiceMock = Mockito.mock(KafkaServiceImpl.class);

	@Test
	void testApplication() throws NoSuchFieldException, IllegalAccessException {
		AISController aisController = new AISController();
		AISService aisService = new AISService();
		Mockito.doNothing().when(kafkaServiceMock).sendAlertMessage(Mockito.anyString(),Mockito.anyString());
		Mockito.doNothing().when(kafkaServiceMock).sendAISTrack(Mockito.any(AISTrackInfoDAO.class));

		ReflectionTestUtils.setField(aisService,"kafkaService", this.kafkaServiceMock);
		ReflectionTestUtils.setField(aisService,"aisTrackMessageRepository", this.aisTrackMessageRepository);
		ReflectionTestUtils.setField(aisService,"aisTrackRepository", this.aisTrackRepository);
		ReflectionTestUtils.setField(aisController,"aisService", aisService);


		List<String> messagesList = new ArrayList<>();
		messagesList.add("!AIVDM,2,1,9,B,59NRrG@25UtPDhiOH00q21<D@<v3O3H00000,0*79");
		messagesList.add("!AIVDM,2,2,9,B,001J2PtDe55P0wDSkPhA3l`0Pp000000000,2*5F");
		messagesList.add("!AIVDM,2,1,0,A,58wt8Ui`g??r21`7S=:22058<v05Htp000000015>8OA;0sk,0*7B");
		messagesList.add("!AIVDM,2,2,0,A,eQ8823mDm3kP00000000000,2*5D");
		messagesList.add("!AIVDM,1,1,,B,19NS7Sp02wo?HETKA2K6mUM20<L=,0*27");
		messagesList.add("!AIVDM,1,1,,B,19NS7Sp02wo?HETKA2K6mUM20<L=,0*27"); // New update for same track above
		List<AISMessage> result = aisMessageDecoderService.decode(messagesList);

		assertEquals("Number of AIS Messages", 4, result.size());

		AISMessage aisMessageShipAndVoyageData = result.get(0);
		aisService.processAISMessage(aisMessageShipAndVoyageData);
		Assert.assertEquals(1, aisTrackRepository.count());
		Assert.assertEquals(1, aisTrackMessageRepository.count());

		Optional<AISTrack> optAisTrack =
			aisTrackRepository.findByMmsi(aisMessageShipAndVoyageData.getSourceMmsi().getMMSI());
		assertTrue(optAisTrack.isPresent());
		AISTrack aisTrack = optAisTrack.get();
		assertEquals("MessageType", AISMessageType.SHIP_AND_VOYAGE_RELATED_DATA, aisTrack.getMessageType());
		assertEquals("MMSI", 636009053, aisTrack.getMMSI());
		assertEquals("Ship Name", "NP SEDCO 706", aisTrack.getShipName());

		AISMessage anotherAISMessage = result.get(1);
		aisService.processAISMessage(anotherAISMessage);
		anotherAISMessage = result.get(2);
		aisService.processAISMessage(anotherAISMessage);
		anotherAISMessage = result.get(3);
		aisService.processAISMessage(anotherAISMessage);

		Assert.assertEquals(3, aisTrackRepository.count());
		Assert.assertEquals(4, aisTrackMessageRepository.count());

		ResponseEntity<HttpStatus> responseEntity = aisController.deleteAISTrackByMmsi(636012431);
		assertEquals(HttpStatus.OK,responseEntity.getStatusCode());

		Assert.assertEquals(2, aisTrackRepository.count());
		Assert.assertEquals(2, aisTrackMessageRepository.count());

		responseEntity = aisController.deleteAllAISTracks();
		assertEquals(HttpStatus.OK,responseEntity.getStatusCode());

		Assert.assertEquals(0, aisTrackRepository.count());
		Assert.assertEquals(0, aisTrackMessageRepository.count());

	}

}

