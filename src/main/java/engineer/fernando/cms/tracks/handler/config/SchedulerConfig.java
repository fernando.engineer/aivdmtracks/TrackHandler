package engineer.fernando.cms.tracks.handler.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.ScheduledMethodRunnable;

import java.util.Date;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

@Configuration
public class SchedulerConfig implements SchedulingConfigurer {

    protected static final Map<Object, ScheduledFuture<?>> scheduledTasks = new IdentityHashMap<>();

    private static final int POOL_SIZE = 100;

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        CustomTaskScheduler threadPoolTaskScheduler = new CustomTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(POOL_SIZE);
        threadPoolTaskScheduler.setThreadNamePrefix("track-task-pool-");
        threadPoolTaskScheduler.initialize();
        scheduledTaskRegistrar.setTaskScheduler(threadPoolTaskScheduler);
    }


    public static class CustomTaskScheduler extends ThreadPoolTaskScheduler {
        @Override
        public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, long period) {
            ScheduledFuture<?> future = super.scheduleAtFixedRate(task, period);
            ScheduledMethodRunnable runnable = (ScheduledMethodRunnable) task;
            synchronized(scheduledTasks) {
                scheduledTasks.put(runnable.getTarget(), future);
            }
            return future;
        }

        @Override
        public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, Date startTime, long period) {
            ScheduledFuture<?> future = super.scheduleAtFixedRate(task, startTime, period);
            ScheduledMethodRunnable runnable = (ScheduledMethodRunnable) task;
            synchronized(scheduledTasks) {
                scheduledTasks.put(runnable.getTarget(), future);
            }
            return future;
        }
    }
}