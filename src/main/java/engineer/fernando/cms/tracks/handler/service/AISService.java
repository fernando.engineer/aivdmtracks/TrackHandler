package engineer.fernando.cms.tracks.handler.service;

import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.cms.tracks.handler.model.dao.AISTrackInfoDAO;
import engineer.fernando.cms.tracks.handler.model.entity.AISTrack;
import engineer.fernando.cms.tracks.handler.model.entity.AISTrackMessage;
import engineer.fernando.cms.tracks.handler.repository.AISTrackMessageRepository;
import engineer.fernando.cms.tracks.handler.repository.AISTrackRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AISService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AISService.class);

	@Autowired
	private AISTrackRepository aisTrackRepository;

	@Autowired
	private AISTrackMessageRepository aisTrackMessageRepository;

	@Autowired
	private KafkaService kafkaService;

	public Flux<AISTrackInfoDAO> produceAISTrackInfoFlux(){
		List<AISTrackInfoDAO> aisTrackInfo = new ArrayList<>();
		List<AISTrack> aisTracks = aisTrackRepository.findAll();
		for (AISTrack aisTrack: aisTracks) {
			List<AISTrackMessage> aisTrackMessages = aisTrackMessageRepository.findByMmsi(aisTrack.getMMSI());
			aisTrackInfo.add(new AISTrackInfoDAO(aisTrack,aisTrackMessages));
		}
		return Flux.fromIterable(aisTrackInfo);
	}

	public void publishKafKaAISTracksInfo(){
		List<AISTrack> aisTracks = aisTrackRepository.findAll();
		for (AISTrack aisTrack: aisTracks) {
			List<AISTrackMessage> aisTrackMessages = aisTrackMessageRepository.findByMmsi(aisTrack.getMMSI());
			kafkaService.sendAISTrack(new AISTrackInfoDAO(aisTrack,aisTrackMessages));
		}
	}

	public void processAISMessage(AISMessage aisMessage) {
		AISTrackMessage aisTrackMessage;
		try {
			// I - A new AISMessageTrack shall be criated
			aisTrackMessage = AISTrackMessage.aisTrackMessageFactory(aisMessage);
			aisTrackMessageRepository.save(aisTrackMessage);
		} catch (ClassNotFoundException | InvocationTargetException | IllegalAccessException e) {
			if(LOGGER.isErrorEnabled()){
				LOGGER.error(
					String.format("AISTrackMessage cannot be processed, aborting.. (AISMessage %s) ",
						aisMessage),
					e);
			}
			return;
		}

		// II - if AISTrack for this MMSI doesn't exists created it, else, update it.
		try {
			Optional<AISTrack> optionalAISTrack =
				aisTrackRepository.findByMmsi(aisMessage.getSourceMmsi().getMMSI());
			String aisTrackInformationAction;
			AISTrack processedAISTrack;
			if(!optionalAISTrack.isPresent()) {
				processedAISTrack = aisTrackRepository.save(AISTrack.aisTrackfactory(aisMessage));
				aisTrackInformationAction = "Received";
			}
			else{
				processedAISTrack = aisTrackRepository.save(optionalAISTrack.get().updateAISTrack(aisMessage));
				aisTrackInformationAction = "Updated";
			}
			String aisTrackInformation =
				String.format("%s AISTrack from %s.", aisTrackInformationAction, aisMessage.getMessageType().getValue());

			// Alert about the creation / update
			kafkaService.sendAlertMessage(
				String.valueOf(aisMessage.getSourceMmsi().getMMSI()),
				aisTrackInformation
			);
			// Get all AISMessages for this AISTrack including the freshly inserted
			List<AISTrackMessage> aisTrackMessages =
				aisTrackMessageRepository.findByMmsi(processedAISTrack.getMMSI());
			// Send the AISTrackInfo from AISTrack updated and its AISTrackMessages
			AISTrackInfoDAO aisTrackInfo =
				new AISTrackInfoDAO(processedAISTrack,aisTrackMessages);
			kafkaService.sendAISTrack(aisTrackInfo);

		} catch (ClassNotFoundException | InvocationTargetException | IllegalAccessException e) {
			if(LOGGER.isErrorEnabled()){
				aisTrackMessageRepository.delete(aisTrackMessage);
				LOGGER.error(
					String.format(
						"AISTrack process failed, aborting/reverting AISTrackMessage creation... (AISMessage %s) ",
						aisMessage),
					e);
			}
		}
	}

	public void deleteAll(){
		aisTrackRepository.deleteAll();
		aisTrackMessageRepository.deleteAll();
	}

	public void deleteByMmsi(long mmsi){
		aisTrackRepository.deleteByMmsi(mmsi);
		aisTrackMessageRepository.deleteByMmsi(mmsi);
	}
}
