package engineer.fernando.cms.tracks.handler.model.entity;

import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.messages.specific.ApplicationSpecificMessage;
import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.CommunicationState;
import engineer.fernando.ais.decoder.messages.types.IMO;
import engineer.fernando.ais.decoder.messages.types.ITDMACommunicationState;
import engineer.fernando.ais.decoder.messages.types.MMSI;
import engineer.fernando.ais.decoder.messages.types.ManeuverIndicator;
import engineer.fernando.ais.decoder.messages.types.NavigationStatus;
import engineer.fernando.ais.decoder.messages.types.PositionFixingDevice;
import engineer.fernando.ais.decoder.messages.types.SOTDMACommunicationState;
import engineer.fernando.ais.decoder.messages.types.ShipType;
import engineer.fernando.ais.decoder.messages.types.SyncState;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@MappedSuperclass
public abstract class AISStructure {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "mmsi")
	private long mmsi;

	@Column(name = "creationTime")
	private long creationTime;
	@Column(name = "messageType")
	private AISMessageType messageType;
	@Column(name = "repeatIndicator")
	private int repeatIndicator;
	@Column(name = "navigationStatus")
	private NavigationStatus navigationStatus;
	@Column(name = "rateOfTurn")
	private int rateOfTurn;
	@Column(name = "speedOverGround")
	private float speedOverGround;
	@Column(name = "positionAccuracy")
	private boolean positionAccuracy;
	@Column(name = "positionAccurate")
	private boolean positionAccurate;
	@Column(name = "latitude")
	private float latitude;
	@Column(name = "longitude")
	private float longitude;
	@Column(name = "courseOverGround")
	private float courseOverGround;
	@Column(name = "trueHeading")
	private int trueHeading;
	@Column(name = "year")
	private int year;
	@Column(name = "month")
	private int month;
	@Column(name = "day")
	private int day;
	@Column(name = "hour")
	private int hour;
	@Column(name = "minute")
	private int minute;
	@Column(name = "second")
	private int second;
	@Column(name = "specialManeuverIndicator")
	private ManeuverIndicator specialManeuverIndicator;
	@Column(name = "raimFlag")
	private boolean raimFlag;
	@Column(name = "communicationState")
	private String communicationState;
	@Column(name = "imo")
	private IMO imo;
	@Column(name = "callsign")
	private String callsign;
	@Column(name = "shipName")
	private String shipName;
	@Column(name = "shipType")
	private ShipType shipType;
	@Column(name = "toBow")
	private int toBow;
	@Column(name = "toStern")
	private int toStern;
	@Column(name = "toStarboard")
	private int toStarboard;
	@Column(name = "toPort")
	private int toPort;
	@Column(name = "positionFixingDevice")
	private PositionFixingDevice positionFixingDevice;
	@Column(name = "etaMonth")
	private int etaMonth;
	@Column(name = "etaDay")
	private int etaDay;
	@Column(name = "etaHour")
	private int etaHour;
	@Column(name = "etaMinute")
	private int etaMinute;
	@Column(name = "draught")
	private float draught;
	@Column(name = "destination")
	private String destination;
	@Column(name = "dataTerminalReady")
	private boolean dataTerminalReady;
	@Column(name = "sequenceNumber")
	private int sequenceNumber;
	@Column(name = "destinationMmsi")
	private MMSI destinationMmsi;
	@Column(name = "retransmit")
	private boolean retransmit;
	@Column(name = "spare")
	private int spare;
	@Column(name = "designatedAreaCode")
	private int designatedAreaCode;
	@Column(name = "functionalId")
	private int functionalId;
	@Column(name = "binaryData",length = 1024)
	private String binaryData;
	@Column(name = "applicationSpecificMessage",length = 1024)
	private ApplicationSpecificMessage applicationSpecificMessage;
	@Column(name = "syncState")
	private SyncState syncState;
	@Column(name = "slotIncrement")
	private int slotIncrement;
	@Column(name = "numberOfSlots")
	private int numberOfSlots;
	@Column(name = "keepFlag")
	private boolean keepFlag;
	@Column(name = "slotTimeout")
	private int slotTimeout;
	@Column(name = "numberOfReceivedStations")
	private int numberOfReceivedStations;
	@Column(name = "slotNumber")
	private int slotNumber;
	@Column(name = "utcHour")
	private int utcHour;
	@Column(name = "utcMinute")
	private int utcMinute;
	@Column(name = "slotOffset")
	private int slotOffset;


	public long getId() {
		return id;
	}

	public long getMMSI() {
		return mmsi;
	}

	public void setMMSI(long mmsi) {
		this.mmsi = mmsi;
	}

	public long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}

	public AISMessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(AISMessageType messageType) {
		this.messageType = messageType;
	}

	public int getRepeatIndicator() {
		return repeatIndicator;
	}

	public void setRepeatIndicator(int repeatIndicator) {
		this.repeatIndicator = repeatIndicator;
	}

	public NavigationStatus getNavigationStatus() {
		return navigationStatus;
	}

	public void setNavigationStatus(NavigationStatus navigationStatus) {
		this.navigationStatus = navigationStatus;
	}

	public int getRateOfTurn() {
		return rateOfTurn;
	}

	public void setRateOfTurn(int rateOfTurn) {
		this.rateOfTurn = rateOfTurn;
	}

	public float getSpeedOverGround() {
		return speedOverGround;
	}

	public void setSpeedOverGround(float speedOverGround) {
		this.speedOverGround = speedOverGround;
	}

	public boolean isPositionAccuracy() {
		return positionAccuracy;
	}

	public void setPositionAccuracy(boolean positionAccuracy) {
		this.positionAccuracy = positionAccuracy;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public float getCourseOverGround() {
		return courseOverGround;
	}

	public void setCourseOverGround(float courseOverGround) {
		this.courseOverGround = courseOverGround;
	}

	public int getTrueHeading() {
		return trueHeading;
	}

	public void setTrueHeading(int trueHeading) {
		this.trueHeading = trueHeading;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	public ManeuverIndicator getSpecialManeuverIndicator() {
		return specialManeuverIndicator;
	}

	public void setSpecialManeuverIndicator(ManeuverIndicator specialManeuverIndicator) {
		this.specialManeuverIndicator = specialManeuverIndicator;
	}

	public boolean isRaimFlag() {
		return raimFlag;
	}

	public void setRaimFlag(boolean raimFlag) {
		this.raimFlag = raimFlag;
	}

	public String getCommunicationState() {
		return communicationState;
	}

	public void setCommunicationState(CommunicationState communicationState) {
		String commType = "null";
		if(communicationState != null){
			this.syncState = communicationState.getSyncState();
			commType = "Undecoded";
			if(communicationState instanceof ITDMACommunicationState){
				ITDMACommunicationState itdmaCommunicationState = (ITDMACommunicationState) communicationState;
				fillITDMACommunicationState(itdmaCommunicationState);
			}
			else if(communicationState instanceof SOTDMACommunicationState){
				commType = "SOTDMACommunicationState";
				SOTDMACommunicationState sotdmaCommunicationState = (SOTDMACommunicationState) communicationState;
				fillSOTDMACommunicationState(sotdmaCommunicationState);
			}
		}
		this.communicationState = commType;
	}

	private void fillITDMACommunicationState(ITDMACommunicationState itdmaCommunicationState) {
		this.communicationState = "ITDMACommunicationState";
		if(itdmaCommunicationState.getSlotIncrement() != null){
			this.slotIncrement = itdmaCommunicationState.getSlotIncrement();
		}
		if(itdmaCommunicationState.getNumberOfSlots() != null){
			this.numberOfSlots = itdmaCommunicationState.getNumberOfSlots();
		}
		if(itdmaCommunicationState.getKeepFlag() != null){
			this.keepFlag = itdmaCommunicationState.getKeepFlag();
		}
	}

	private void fillSOTDMACommunicationState(SOTDMACommunicationState sotdmaCommunicationState) {
		if(sotdmaCommunicationState.getSlotTimeout() != null){
			this.slotTimeout = sotdmaCommunicationState.getSlotTimeout();
		}
		if(sotdmaCommunicationState.getNumberOfReceivedStations() != null){
			this.numberOfReceivedStations = sotdmaCommunicationState.getNumberOfReceivedStations();
		}
		if(sotdmaCommunicationState.getSlotNumber() != null){
			this.slotNumber = sotdmaCommunicationState.getSlotNumber();
		}
		if(sotdmaCommunicationState.getUtcHour() != null){
			this.utcHour = sotdmaCommunicationState.getUtcHour();
		}
		if(sotdmaCommunicationState.getUtcMinute() != null){
			this.utcMinute = sotdmaCommunicationState.getUtcMinute();
		}
		if(sotdmaCommunicationState.getSlotOffset() != null){
			this.slotOffset = sotdmaCommunicationState.getSlotOffset();
		}
	}

	public IMO getImo() {
		return imo;
	}

	public void setImo(IMO imo) {
		this.imo = imo;
	}

	public String getCallsign() {
		return callsign;
	}

	public void setCallsign(String callsign) {
		this.callsign = callsign;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public ShipType getShipType() {
		return shipType;
	}

	public void setShipType(ShipType shipType) {
		this.shipType = shipType;
	}

	public Integer getToBow() {
		return toBow;
	}

	public void setToBow(Integer toBow) {
		this.toBow = toBow;
	}

	public Integer getToStern() {
		return toStern;
	}

	public void setToStern(Integer toStern) {
		this.toStern = toStern;
	}

	public Integer getToStarboard() {
		return toStarboard;
	}

	public void setToStarboard(Integer toStarboard) {
		this.toStarboard = toStarboard;
	}

	public Integer getToPort() {
		return toPort;
	}

	public void setToPort(Integer toPort) {
		this.toPort = toPort;
	}

	public PositionFixingDevice getPositionFixingDevice() {
		return positionFixingDevice;
	}

	public void setPositionFixingDevice(PositionFixingDevice positionFixingDevice) {
		this.positionFixingDevice = positionFixingDevice;
	}

	public Integer getEtaMonth() {
		return etaMonth;
	}

	public void setEtaMonth(Integer etaMonth) {
		this.etaMonth = etaMonth;
	}

	public Integer getEtaDay() {
		return etaDay;
	}

	public void setEtaDay(Integer etaDay) {
		this.etaDay = etaDay;
	}

	public Integer getEtaHour() {
		return etaHour;
	}

	public void setEtaHour(Integer etaHour) {
		this.etaHour = etaHour;
	}

	public Integer getEtaMinute() {
		return etaMinute;
	}

	public void setEtaMinute(Integer etaMinute) {
		this.etaMinute = etaMinute;
	}

	public Float getDraught() {
		return draught;
	}

	public void setDraught(Float draught) {
		this.draught = draught;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Boolean getDataTerminalReady() {
		return dataTerminalReady;
	}

	public void setDataTerminalReady(Boolean dataTerminalReady) {
		this.dataTerminalReady = dataTerminalReady;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public MMSI getDestinationMmsi() {
		return destinationMmsi;
	}

	public void setDestinationMmsi(MMSI destinationMmsi) {
		this.destinationMmsi = destinationMmsi;
	}

	public boolean isRetransmit() {
		return retransmit;
	}

	public void setRetransmit(boolean retransmit) {
		this.retransmit = retransmit;
	}

	public int getSpare() {
		return spare;
	}

	public void setSpare(int spare) {
		this.spare = spare;
	}

	public int getDesignatedAreaCode() {
		return designatedAreaCode;
	}

	public void setDesignatedAreaCode(int designatedAreaCode) {
		this.designatedAreaCode = designatedAreaCode;
	}

	public int getFunctionalId() {
		return functionalId;
	}

	public void setFunctionalId(int functionalId) {
		this.functionalId = functionalId;
	}

	public String getBinaryData() {
		return binaryData;
	}

	public void setBinaryData(String binaryData) {
		this.binaryData = binaryData;
	}

	public ApplicationSpecificMessage getApplicationSpecificMessage() {
		return applicationSpecificMessage;
	}

	public void setApplicationSpecificMessage(ApplicationSpecificMessage applicationSpecificMessage) {
		this.applicationSpecificMessage = applicationSpecificMessage;
	}

	static List<Method> getAISMessageGetters(AISMessage aisMessage) throws ClassNotFoundException {
		// Determine AISMessage specialized class according its Type
		Integer aisMessageTypeCode = aisMessage.getMessageType().getCode();
		var classLoader = AISMessageType.AIS_MESSAGES_CLASSES.get(aisMessageTypeCode).getClassLoader();
		Class<AISMessage> clazz = null;
		clazz = (Class<AISMessage>) classLoader.loadClass(AISMessageType.AIS_MESSAGES_CLASSES.get(aisMessageTypeCode).getName());
		List<Method> aisMessageGetters = new ArrayList<>();
		// While exists superclass search for and create a list of all Class<AISMessage> getters
		while(clazz.getSuperclass() != null){
			aisMessageGetters.addAll(
				List.of(clazz.getDeclaredMethods())
					.stream()
					.filter(getmethod -> getmethod.getName().startsWith("get"))
					.collect(Collectors.toUnmodifiableList()));
			// Search into superclass upper
			clazz = (Class<AISMessage>) clazz.getSuperclass();
		}
		return aisMessageGetters;
	}

	static List<Method> getAISStructureSetters(AISStructure aisStructure){
		List<Method> aisMessageSetters = new ArrayList<>();
		// While exists superclass search for and create a list of all Class<AISStructure> getters
		Class<AISStructure> clazz = (Class<AISStructure>) aisStructure.getClass();
		while(clazz.getSuperclass() != null){
			aisMessageSetters.addAll(
				List.of(clazz.getDeclaredMethods())
					.stream()
					.filter(getmethod -> getmethod.getName().startsWith("set"))
					.collect(Collectors.toUnmodifiableList()));
			// Search into superclass upper
			clazz = (Class<AISStructure>) clazz.getSuperclass();
		}
		return aisMessageSetters;
	}

	/**
	 * For Each AISTrackMessage setter,
	 * if specialized AISMessage contains a getter matching the same attribute name,
	 * invoke that setter over aisTrackMessage obj passing the respective aisMessage obj getter value
	 */
	static AISStructure dynamicInjector(AISStructure aisStructure, AISMessage aisMessage)
		throws ClassNotFoundException, InvocationTargetException, IllegalAccessException {

		// Create a list of all Class<AISMessage> getters
		List<Method> getters = getAISMessageGetters(aisMessage);

		// Create a list of all AISStructure setters
		List<Method> setters = getAISStructureSetters(aisStructure);

		for (Method setterMethod : setters){
			String param = setterMethod.getName().split("set")[1];
			String getMethodName = "get".concat(param);
			Optional<Method> optGetMethod = getters.stream().filter(m -> m.getName().equals(getMethodName)).findFirst();
			if(optGetMethod.isPresent()){
				Method aisGetMethod = optGetMethod.get();
				if(aisGetMethod.invoke(aisMessage) != null){
					setterMethod.invoke(aisStructure,aisGetMethod.invoke(aisMessage));
				}
			}
		}
		return aisStructure;
	}

	@Override
	public String toString() {
		return String.format("AISStructure [creationTime=%s, id=%s, mmsi=%s, messageType=%s]",
			this.getCreationTime(),
			this.getId(),
			this.getMMSI(),
			this.getMessageType().getValue()
		);
	}

}
