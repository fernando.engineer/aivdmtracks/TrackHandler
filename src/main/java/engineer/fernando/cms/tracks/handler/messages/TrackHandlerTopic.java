package engineer.fernando.cms.tracks.handler.messages;

public enum TrackHandlerTopic {
    TRACKDAO_TOPIC(1),
    TACTICAL_ALERT_TOPIC(2),
    SYSTEM_ALERT(3),
    TACTICAL_UPDATE(4),
    TRACKDAO_PERFORMANCE_TOPIC(5),
    AISMESSAGE_TOPIC(6);

    TrackHandlerTopic(Integer code) {
		this.code = code;
    }

    private final Integer code;

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return toString();
    }

}
