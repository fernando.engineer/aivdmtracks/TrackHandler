package engineer.fernando.cms.tracks.handler.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Value("${spring.rabbitmq.queue.systemtrack}")
    private String queue;

    @Bean
    public Queue queue(){
        return new Queue(queue,false);
    }

    @Value("${spring.rabbitmq.queue.nmea}")
    private String nmeaQueue;

    @Bean
    public Queue nmeaQueue(){
        return new Queue(nmeaQueue,false);
    }

    @Bean
    public Jackson2JsonMessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }
}
