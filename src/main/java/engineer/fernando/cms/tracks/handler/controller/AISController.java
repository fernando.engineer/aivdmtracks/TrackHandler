package engineer.fernando.cms.tracks.handler.controller;

import engineer.fernando.cms.tracks.handler.service.AISService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@Component
@RestController
@CrossOrigin
public class AISController {

    @Autowired
    private AISService aisService;

    @DeleteMapping("/aistracks")
    public ResponseEntity<HttpStatus> deleteAllAISTracks() {
        try {
            aisService.deleteAll();
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/aistracks/{mmsi}")
    public ResponseEntity<HttpStatus> deleteAISTrackByMmsi(@PathVariable("mmsi") long mmsi) {
        try {
            aisService.deleteByMmsi(mmsi);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
