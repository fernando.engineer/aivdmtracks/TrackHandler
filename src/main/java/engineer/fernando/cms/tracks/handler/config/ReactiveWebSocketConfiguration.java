package engineer.fernando.cms.tracks.handler.config;

import engineer.fernando.cms.tracks.handler.controller.AISWebSocketHandler;
import engineer.fernando.cms.tracks.handler.controller.KafkaWebSocketHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ReactiveWebSocketConfiguration {

    @Bean
    public HandlerMapping webSocketHandlerMapping(KafkaWebSocketHandler kafkaWebSocketHandler, AISWebSocketHandler aisWebSocketHandler) {
        Map<String, WebSocketHandler> urlMap = new HashMap<>();
        urlMap.put("/websocket", kafkaWebSocketHandler);
        urlMap.put("/aissocket", aisWebSocketHandler);

        Map<String, CorsConfiguration> corsConfigurationMap =
                new HashMap<>();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*");
        corsConfigurationMap.put("/websocket", corsConfiguration);
        corsConfigurationMap.put("/aissocket", corsConfiguration);

        SimpleUrlHandlerMapping handlerMapping = new SimpleUrlHandlerMapping();
        handlerMapping.setOrder(1);
        handlerMapping.setUrlMap(urlMap);
        return handlerMapping;
    }

    @Bean
    public WebSocketHandlerAdapter handlerAdapter() {
        return new WebSocketHandlerAdapter();
    }
}