package engineer.fernando.cms.tracks.handler.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import engineer.fernando.cms.domainmodel.avro.SensorTrackType;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Entity
@Table(name = "systemtracks")
public class SystemTrack implements Serializable {
    @Id
    @SequenceGenerator(name = "systemtracks", sequenceName = "SYSTEMTRACKSEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "systemtracks")
    private long id;
    @Column(name = "systemTrackId")
    private String systemTrackId;
    @Column(name = "sensorTrackType")
    private SensorTrackType sensorTrackType;
    @Column(name = "playerId")
    private long playerId;

    @OneToOne(mappedBy = "systemTrack", cascade = CascadeType.ALL)
    @JsonManagedReference
    @PrimaryKeyJoinColumn
    private TrackSpecification specification;

    @Column(name = "name")
    private String name;
    @Column(name = "orderedSpeedOverGround")
    private double orderedSpeedOverGround;
    @Column(name = "orderedCourseOverGround")
    private double orderedCourseOverGround;
    @Column(name = "weaponsAvailable")
    private int weaponsAvailable;
    @Column(name = "creationTime")
    private long creationTime;
    @Column(name = "hit")
    private boolean hit;

    @OneToMany(mappedBy = "systemTrack", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Column(nullable = true)
    @JsonManagedReference
    private List<TrackTacticalSituation> tacticalUpdates = new ArrayList<>();

    public long getId() {
        return id;
    }

    public String getSystemTrackId() {
        return systemTrackId;
    }

    public void setSystemTrackId(String systemTrackId) {
        this.systemTrackId = systemTrackId;
    }

    public SensorTrackType getSensorTrackType() {
        return sensorTrackType;
    }

    public void setSensorTrackType(SensorTrackType sensorTrackType) { this.sensorTrackType = sensorTrackType;}

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public long getPlayerId() { return playerId;}

    public void setPlayerId(long playerId) {this.playerId = playerId;}

    public TrackSpecification getTrackSpecification() {
        return specification;
    }

    public void setTrackSpecification(TrackSpecification techSpec) {
        this.specification = techSpec;
        techSpec.setSystemTrack(this);
    }

    public int getWeaponsAvailable() { return weaponsAvailable;}

    public void setWeaponsAvailable(int weaponsAvailable) { this.weaponsAvailable = weaponsAvailable;}

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    public double getOrderedCourseOverGround() { return orderedCourseOverGround;}

    public void setOrderedCourseOverGround(double orderedCourseOverGround) {
        this.orderedCourseOverGround = Math.round(orderedCourseOverGround * 100.0) / 100.0;
    }

    public double getOrderedSpeedOverGround() { return orderedSpeedOverGround;}

    public void setOrderedSpeedOverGround(double orderedSpeedOverGround) {
        this.orderedSpeedOverGround = Math.round(orderedSpeedOverGround * 100.0) / 100.0;
    }

    public boolean isHit(){
        return this.hit;
    }

    public void setHit(boolean hit){
        this.hit = hit;
    }

    public List<TrackTacticalSituation> getTacticalUpdates(){
        tacticalUpdates.sort(Comparator.comparing(TrackTacticalSituation::getCreationTime).reversed());
        return tacticalUpdates;
    }

    public void addTacticalUpdate(TrackTacticalSituation tacticalSituationUpdate) {
        tacticalUpdates.add(tacticalSituationUpdate);
    }

    public TrackTacticalSituation getLastTacticalUpdate(){
        List<TrackTacticalSituation> updates = getTacticalUpdates();
        if(!updates.isEmpty() ){
            return getTacticalUpdates().get(0);
        }
        return null;
    }

    public void setLastTacticalUpdate(TrackTacticalSituation tacticalSituationUpdate) {
        getTacticalUpdates().set(0,tacticalSituationUpdate);
    }


    public void removeTacticalUpdate(TrackTacticalSituation tacticalSituationUpdate) {
        tacticalUpdates.remove(tacticalSituationUpdate);
        tacticalSituationUpdate.setSystemTrack(null);
    }

    @Override
    public String toString() {
        return "SystemTrack [id=" + id + ", systemTrackId=" + systemTrackId + ", sensorTrackType=" + sensorTrackType
                + ", name=" + name + ", playerId=" + playerId + ",OrderedCoG="+ orderedCourseOverGround
                + ", OrderedSoG=" +orderedSpeedOverGround + ", WeaponsAvailable=" + weaponsAvailable
                + ", specification=" + specification + "Hit?:" + hit +", CreationTime=" +creationTime +" received.]";
    }

}