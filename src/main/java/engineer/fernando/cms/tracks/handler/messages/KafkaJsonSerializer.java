package engineer.fernando.cms.tracks.handler.messages;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class KafkaJsonSerializer implements Serializer<TrackHandlerMessage> {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaJsonSerializer.class);

    @Override
    public void configure(Map map, boolean b) {
        // Nothing to Do but Serializer interface requires it.
    }

    @Override
    public byte[] serialize(String s, TrackHandlerMessage trackHandlerMessage) {
        byte[] retVal = null;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        try {
            retVal = objectMapper.writeValueAsBytes(trackHandlerMessage);
        } catch (Exception e) {
            if(LOGGER.isErrorEnabled()){
                LOGGER.error("Error: ", e);
            }
        }
        return retVal;
    }

    @Override
    public void close() {
        // Nothing to Do but Serializer interface requires it.
    }
}