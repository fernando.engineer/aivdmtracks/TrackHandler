package engineer.fernando.cms.tracks.handler.model.dao;

import engineer.fernando.cms.tracks.handler.model.entity.AISTrack;
import engineer.fernando.cms.tracks.handler.model.entity.AISTrackMessage;

import java.io.Serializable;
import java.util.List;

public class AISTrackInfoDAO implements Serializable  {

	private transient AISTrack aisTrack;
	private transient List<AISTrackMessage> aisTrackMessages;

	public AISTrackInfoDAO(AISTrack aisTrack, List<AISTrackMessage> aisTrackMessages){
		this.aisTrack = aisTrack;
		this.aisTrackMessages = aisTrackMessages;
	}

	public AISTrack getAisTrack() {
		return aisTrack;
	}

	public List<AISTrackMessage> getAisTrackMessages() {
		return aisTrackMessages;
	}

}
