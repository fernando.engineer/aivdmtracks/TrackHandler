package engineer.fernando.cms.tracks.handler.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import engineer.fernando.cms.domainmodel.avro.TrackType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "systemtracks_spec")
public class TrackSpecification implements Serializable {

    @Id
    @Column(name = "systemTrackId")
    private long id;

    @OneToOne
    @MapsId
    @JoinColumn(name="systemTrackId", referencedColumnName="systemTrackId", foreignKey=@ForeignKey(name = "FK_systemTrack_spec"))
    @JsonBackReference
    private SystemTrack systemTrack;

    @Column(name = "trackType")
    private TrackType trackType;
    @Column(name = "dim_beam")
    private double beam;
    @Column(name = "dim_loa")
    private double loa;
    @Column(name = "dim_draught")
    private double draught;
    @Column(name = "dim_grossTonnage")
    private double grossTonnage;
    @Column(name = "dim_netTonnage")
    private double netTonnage;
    @Column(name = "maxSpeedOverGround")
    private double maxSpeedOverGround;
    @Column(name = "angularAcceleration")
    private double angularAcceleration;
    @Column(name = "acceleration")
    private double acceleration;
    @Column(name = "weaponsCapacity")
    private int weaponsCapacity;
    @Column(name = "fuelCapacityInGallons")
    private double fuelCapacityInGallons;
    @Column(name = "noiseDueSpeedLinearCoefficient")
    private double noiseDueSpeedLinearCoefficient;
    @Column(name = "noiseDueSpeedQuadraticCoefficient")
    private double  noiseDueSpeedQuadraticCoefficient;
    @Column(name = "sonarDetectionRange")
    private double sonarDetectionRange;
    @Column(name = "sonarDetectionAzimuthRange")
    private double sonarDetectionAzimuthRange;
    @Column(name = "radarTransmittedPulsePower")
    private double radarTransmittedPulsePower;
    @Column(name = "radarDetectionRange")
    private double radarDetectionRange;
    @Column(name = "radarDetectionAzimuthRange")
    private double radarDetectionAzimuthRange;
    @Column(name = "radarAntennaTurnSpeed")
    private double radarAntennaTurnSpeed;
    @Column(name = "directivityDetectionGain")
    private double directivityDetectionGain;
    @Column(name = "directivityDetectionAzimuth")
    private double directivityDetectionAzimuth;

    public TrackSpecification(TrackSpecification trackSpecification) {
        this.setSystemTrack(trackSpecification.systemTrack);
        this.setTrackType(trackSpecification.trackType);
        this.setBeam(trackSpecification.beam);
        this.setLoa(trackSpecification.loa);
        this.setDraught(trackSpecification.draught);
        this.setGrossTonnage(trackSpecification.grossTonnage);
        this.setNetTonnage(trackSpecification.netTonnage);
        this.setMaxSpeedOverGround(trackSpecification.maxSpeedOverGround);
        this.setAngularAcceleration(trackSpecification.angularAcceleration);
        this.setAcceleration(trackSpecification.acceleration);
        this.setWeaponsCapacity(trackSpecification.weaponsCapacity);
        this.setFuelCapacityInGallons(trackSpecification.fuelCapacityInGallons);
        this.setNoiseDueSpeedLinearCoefficient(trackSpecification.noiseDueSpeedLinearCoefficient);
        this.setNoiseDueSpeedQuadraticCoefficient(trackSpecification.noiseDueSpeedQuadraticCoefficient);
        this.setSonarDetectionRange(trackSpecification.sonarDetectionRange);
        this.setSonarDetectionAzimuthRange(trackSpecification.sonarDetectionAzimuthRange);
        this.setRadarTransmittedPulsePower(trackSpecification.radarTransmittedPulsePower);
        this.setRadarDetectionRange(trackSpecification.radarDetectionRange);
        this.setRadarDetectionAzimuthRange(trackSpecification.radarDetectionAzimuthRange);
        this.setRadarAntennaTurnSpeed(trackSpecification.radarAntennaTurnSpeed);
        this.setDirectivityDetectionGain(trackSpecification.directivityDetectionGain);
        this.setDirectivityDetectionAzimuth(trackSpecification.directivityDetectionAzimuth);
    }

    public TrackSpecification() {
    }

    public long getId() {
        return id;
    }

    public SystemTrack getSystemTrack() {
        return systemTrack;
    }

    public void setSystemTrack(SystemTrack systemTrack) {
        this.systemTrack = systemTrack;
    }

    public TrackType getTrackType() {
        return trackType;
    }

    public void setTrackType(TrackType trackType) {
        this.trackType = trackType;
    }

    public double getBeam() {
        return beam;
    }

    public void setBeam(double beam) {
        this.beam = beam;
    }

    public double getLoa() {
        return loa;
    }

    public void setLoa(double loa) {
        this.loa = loa;
    }

    public double getDraught() {
        return draught;
    }

    public void setDraught(double draught) {
        this.draught = draught;
    }

    public double getGrossTonnage() {
        return grossTonnage;
    }

    public void setGrossTonnage(double grossTonnage) {
        this.grossTonnage = grossTonnage;
    }

    public double getNetTonnage() {
        return netTonnage;
    }

    public void setNetTonnage(double netTonnage) {
        this.netTonnage = netTonnage;
    }

    public double getMaxSpeedOverGround() {
        return maxSpeedOverGround;
    }

    public void setMaxSpeedOverGround(double maxSpeedOverGround) {
        this.maxSpeedOverGround = maxSpeedOverGround;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }

    public double getAngularAcceleration() {
        return angularAcceleration;
    }

    public void setAngularAcceleration(double angularAcceleration) {
        this.angularAcceleration = angularAcceleration;
    }

    public int getWeaponsCapacity() {
        return weaponsCapacity;
    }

    public void setWeaponsCapacity(int weaponsCapacity) {
        this.weaponsCapacity = weaponsCapacity;
    }

    public double getFuelCapacityInGallons() {
        return fuelCapacityInGallons;
    }

    public double getNoiseDueSpeedLinearCoefficient() {
        return noiseDueSpeedLinearCoefficient;
    }

    public void setNoiseDueSpeedLinearCoefficient(double noiseDueSpeedLinearCoefficient) {
        this.noiseDueSpeedLinearCoefficient = noiseDueSpeedLinearCoefficient;
    }

    public double getNoiseDueSpeedQuadraticCoefficient() {
        return noiseDueSpeedQuadraticCoefficient;
    }

    public void setNoiseDueSpeedQuadraticCoefficient(double noiseDueSpeedQuadraticCoefficient) {
        this.noiseDueSpeedQuadraticCoefficient = noiseDueSpeedQuadraticCoefficient;
    }

    public void setFuelCapacityInGallons(double fuelCapacityInGallons) {
        this.fuelCapacityInGallons = fuelCapacityInGallons;
    }

    public double getSonarDetectionRange() {
        return sonarDetectionRange;
    }

    public void setSonarDetectionRange(double sonarDetectionRange) {
        this.sonarDetectionRange = sonarDetectionRange;
    }

    public double getSonarDetectionAzimuthRange() {
        return sonarDetectionAzimuthRange;
    }

    public void setSonarDetectionAzimuthRange(double sonarDetectionAzimuthRange) {
        this.sonarDetectionAzimuthRange = sonarDetectionAzimuthRange;
    }

    public double getRadarTransmittedPulsePower() {
        return radarTransmittedPulsePower;
    }

    public void setRadarTransmittedPulsePower(double radarTransmittedPulsePower) {
        this.radarTransmittedPulsePower = radarTransmittedPulsePower;
    }

    public double getRadarDetectionRange() {
        return radarDetectionRange;
    }

    public void setRadarDetectionRange(double radarDetectionRange) {
        this.radarDetectionRange = radarDetectionRange;
    }

    public double getRadarDetectionAzimuthRange() {
        return radarDetectionAzimuthRange;
    }

    public void setRadarDetectionAzimuthRange(double radarDetectionAzimuthRange) {
        this.radarDetectionAzimuthRange = radarDetectionAzimuthRange;
    }

    public double getRadarAntennaTurnSpeed() {
        return radarAntennaTurnSpeed;
    }

    public void setRadarAntennaTurnSpeed(double radarAntennaTurnSpeed) {
        this.radarAntennaTurnSpeed = radarAntennaTurnSpeed;
    }

    public double getDirectivityDetectionGain() {
        return directivityDetectionGain;
    }

    public void setDirectivityDetectionGain(double directivityDetectionGain) {
        this.directivityDetectionGain = directivityDetectionGain;
    }

    public double getDirectivityDetectionAzimuth() {
        return directivityDetectionAzimuth;
    }

    public void setDirectivityDetectionAzimuth(double directivityDetectionAzimuth) {
        this.directivityDetectionAzimuth = directivityDetectionAzimuth;
    }

    @Override
    public String toString() {
        return "TrackSpecification [id=" + id
                + ", trackType=" + trackType + ", beam=" + beam + ", loa=" + loa + ",draught="+ draught
                + ", grossTonnage=" +grossTonnage + ", netTonnage=" + netTonnage
                + ", maxSpeedOverGround=" + maxSpeedOverGround + ", acceleration:" + acceleration
                + ", angularAcceleration=" + angularAcceleration + ", weaponsCapacity=" + weaponsCapacity
                + ", fuelCapacityInGallons=" +fuelCapacityInGallons
                + ", noiseDueSpeedLinearCoefficient=" + noiseDueSpeedLinearCoefficient
                + ", noiseDueSpeedQuadraticCoefficient=" + noiseDueSpeedQuadraticCoefficient
                + ", sonarDetectionRange:" + sonarDetectionRange
                + ", sonarDetectionAzimuthRange=" + sonarDetectionAzimuthRange
                + ", radarTransmittedPulsePower:" + radarTransmittedPulsePower
                + ", radarDetectionRange=" + radarDetectionRange
                + ", radarDetectionAzimuthRange:" + radarDetectionAzimuthRange
                + ", radarAntennaTurnSpeed=" + radarAntennaTurnSpeed
                + ", directivityDetectionGain:" + directivityDetectionGain
                + ", directivityDetectionAzimuth=" + directivityDetectionAzimuth
                + ", weaponsCapacity=" + weaponsCapacity + ", received.]";

    }
}