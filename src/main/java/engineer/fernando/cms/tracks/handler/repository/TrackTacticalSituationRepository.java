package engineer.fernando.cms.tracks.handler.repository;

import engineer.fernando.cms.tracks.handler.model.entity.SystemTrack;
import engineer.fernando.cms.tracks.handler.model.entity.TrackTacticalSituation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import javax.transaction.Transactional;

import java.util.List;
import java.util.Optional;

public interface TrackTacticalSituationRepository extends JpaRepository<TrackTacticalSituation,Long> {

    List<TrackTacticalSituation> findBySystemTrack(SystemTrack systemTrack);

    @Query("SELECT c FROM TrackTacticalSituation c WHERE c.systemTrack = ?1 AND c.creationTime in ( SELECT MAX(creationTime) FROM TrackTacticalSituation group by systemTrack having count(systemTrack)>0 )")
    Optional<TrackTacticalSituation> findLastUpdateBySystemTrack(SystemTrack systemTrack);

    @Transactional
    void deleteBySystemTrack(SystemTrack systemTrack);

}
