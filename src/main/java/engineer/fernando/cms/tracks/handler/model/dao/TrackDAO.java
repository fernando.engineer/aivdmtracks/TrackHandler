package engineer.fernando.cms.tracks.handler.model.dao;

import engineer.fernando.cms.domainmodel.avro.SensorTrackType;

import java.io.Serializable;

public class TrackDAO implements Serializable {

    private String trackId;
    private long playerId;
    private SensorTrackType sensorTrackType;
    private String name;
    private double orderedSpeedOverGround;
    private double orderedCourseOverGround;
    private double latitude;
    private double longitude;
    private double projetedLatitude;
    private double projectedLongitude;
    private double speedOverGround;
    private double courseOverGround;
    private double distanceTravelled;
    private double fuelInGallons;
    private double dBNoise;
    private int weaponsAvailable;
    private boolean hit;
    private long creationTime;
    private boolean radarAntennaTurnStatus;
    private boolean radarStatus;
    private double radarAzimuth;

    private long updatedTime;

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public SensorTrackType getSensorTrackType() {
        return sensorTrackType;
    }

    public void setSensorTrackType(SensorTrackType sensorTrackType) {
        this.sensorTrackType = sensorTrackType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getOrderedSpeedOverGround() {
        return orderedSpeedOverGround;
    }

    public void setOrderedSpeedOverGround(double orderedSpeedOverGround) {
        this.orderedSpeedOverGround = orderedSpeedOverGround;
    }

    public double getOrderedCourseOverGround() {
        return orderedCourseOverGround;
    }

    public void setOrderedCourseOverGround(double orderedCourseOverGround) {
        this.orderedCourseOverGround = orderedCourseOverGround;
    }

    public double getDistanceTravelled() { return distanceTravelled; }

    public void setDistanceTravelled(double distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getProjetedLatitude() {
        return projetedLatitude;
    }

    public void setProjetedLatitude(double projetedLatitude) {
        this.projetedLatitude = projetedLatitude;
    }

    public double getProjectedLongitude() {
        return projectedLongitude;
    }

    public void setProjectedLongitude(double projectedLongitude1hour) {
        this.projectedLongitude = projectedLongitude1hour;
    }

    public double getSpeedOverGround() {
        return speedOverGround;
    }

    public void setSpeedOverGround(double speedOverGround) {
        this.speedOverGround = speedOverGround;
    }

    public double getCourseOverGround() {
        return courseOverGround;
    }

    public void setCourseOverGround(double courseOverGround) {
        this.courseOverGround = courseOverGround;
    }

    public double getFuelInGallons() {
        return fuelInGallons;
    }

    public void setFuelInGallons(double fuelInGallons) {
        this.fuelInGallons = fuelInGallons;
    }

    public double getdBNoise() {
        return dBNoise;
    }

    public void setdBNoise(double dBNoise) {
        this.dBNoise = dBNoise;
    }

    public int getWeaponsAvailable() {
        return weaponsAvailable;
    }

    public void setWeaponsAvailable(int weaponsAvailable) {
        this.weaponsAvailable = weaponsAvailable;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    public long getCreationTime() {
        return creationTime;
    }


    public boolean isRadarAntennaTurnStatus() {
        return radarAntennaTurnStatus;
    }

    public void setRadarAntennaTurnStatus(boolean radarAntennaTurnStatus) {
        this.radarAntennaTurnStatus = radarAntennaTurnStatus;
    }

    public boolean isRadarStatus() {
        return radarStatus;
    }

    public void setRadarStatus(boolean radarStatus) {
        this.radarStatus = radarStatus;
    }

    public double getRadarAzimuth() {
        return radarAzimuth;
    }

    public void setRadarAzimuth(double radarAzimuth) {
        this.radarAzimuth = radarAzimuth;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }


    public long getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime) {
        this.updatedTime = updatedTime;
    }


    @Override
    public String toString() {
        return "TrackDAO [TrackId=" + trackId + ", sensorTrackType=" + sensorTrackType + ", name=" + name
                + ", playerId=" + playerId + ", Lat/Lng=(" + latitude + "," + longitude+
                "), distanceTravelled=" + distanceTravelled + ", CoG="+ courseOverGround
                + ",OrderedCoG="+ orderedCourseOverGround + ", SoG=" +speedOverGround
                + ", OrderedSoG=" +orderedSpeedOverGround + ", FuelInGallons=" + fuelInGallons + ", NoiseInDb=" +dBNoise
                + ", WeaponsAvailable=" + weaponsAvailable + "Hit?:" + hit + ", CreationTime=" +creationTime
                +", UpdatedTime=" +updatedTime +".]";
    }
}
