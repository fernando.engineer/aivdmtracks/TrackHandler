package engineer.fernando.cms.tracks.handler.model.dao;

public class TrackTacticalUpdateDAOBuilder {

    private String trackId;
    private double latitude;
    private double longitude;
    private double speedOverGround;
    private double courseOverGround;
    private double fuelInGallons;
    private double noiseInDb;
    private long creationTime;

    public TrackTacticalUpdateDAOBuilder setTrackId(String trackId) {
        this.trackId = trackId;
        return this;
    }


    public TrackTacticalUpdateDAOBuilder setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public TrackTacticalUpdateDAOBuilder setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public TrackTacticalUpdateDAOBuilder setSpeedOverGround(double speedOverGround) {
        this.speedOverGround = speedOverGround;
        return this;
    }

    public TrackTacticalUpdateDAOBuilder setCourseOverGround(double courseOverGround) {
        this.courseOverGround = courseOverGround;
        return this;
    }

    public TrackTacticalUpdateDAOBuilder setFuelInGallons(double fuelInGallons)
    {
        this.fuelInGallons = fuelInGallons;
        return this;
    }

    public TrackTacticalUpdateDAOBuilder setNoiseInDb(double dBNoise) {
        this.noiseInDb = dBNoise;
        return this;
    }

    public TrackTacticalUpdateDAOBuilder setCreationTime(long creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public TrackTacticalUpdateDAO build(){
        TrackTacticalUpdateDAO trackTacticalUpdateDAO = new TrackTacticalUpdateDAO();
        trackTacticalUpdateDAO.setTrackId(this.trackId);
        trackTacticalUpdateDAO.setLatitude(this.latitude);
        trackTacticalUpdateDAO.setLongitude(this.longitude);
        trackTacticalUpdateDAO.setSpeedOverGround(this.speedOverGround);
        trackTacticalUpdateDAO.setCourseOverGround(this.courseOverGround);
        trackTacticalUpdateDAO.setCreationTime(this.creationTime);
        trackTacticalUpdateDAO.setFuelInGallons(this.fuelInGallons);
        trackTacticalUpdateDAO.setNoiseInDb(this.noiseInDb);
        return trackTacticalUpdateDAO;
    }
}
