package engineer.fernando.cms.tracks.handler.model.entity;

import engineer.fernando.ais.decoder.messages.AISMessage;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.lang.reflect.InvocationTargetException;


@Entity
@Table(name = "aismessage")
public class AISTrackMessage extends AISStructure {

	public static AISTrackMessage aisTrackMessageFactory(AISMessage aisMessage)
		throws ClassNotFoundException, InvocationTargetException, IllegalAccessException {
		AISTrackMessage aisTrackMessage = new AISTrackMessage();
		aisTrackMessage.setMMSI(aisMessage.getSourceMmsi().getMMSI());
		aisTrackMessage.setCreationTime(System.currentTimeMillis());
		//Perform dynamic injection on aisTrackMessage obj matching its setters to respective aisMessage getters
		return (AISTrackMessage) dynamicInjector(aisTrackMessage, aisMessage);
	}

	@Override
	public String toString() {
		return String.format("AISMessage [creationTime=%s, id=%s, mmsi=%s, messageType=%s]",
			this.getCreationTime(),
			this.getId(),
			this.getMMSI(),
			this.getMessageType().getValue()
		);
	}

}
